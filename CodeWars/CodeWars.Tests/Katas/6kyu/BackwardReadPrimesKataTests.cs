﻿using CodeWars.Katas._6kyu;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeWars.Tests.Katas._6kyu
{
    [TestFixture]
    public class BackwardReadPrimesKataTests
    {
        private static readonly List<KataSource> SourceList = new List<KataSource>()
        {
            new KataSource() {Min = 1, Max = 100, ExpectedResult = new List<int>() {13, 17, 31, 37, 71, 73, 79, 97 }},
            new KataSource() {Min = 9900, Max = 10000, ExpectedResult = new List<int>() {9923, 9931, 9941, 9967 }}
        };

        [Test, TestCaseSource(nameof(SourceList))]
        public void Return_BackwardReadPrimes(KataSource source)
        {
            Assert.AreEqual(string.Join(" ", source.ExpectedResult), BackwardReadPrimesKata.BackwardsPrime(source.Min, source.Max),
                "Returned wrong numbers. Case: ({0},{1})", source.Min, source.Max);
        }
    }

    public class KataSource
    {
        public List<int> ExpectedResult { get; set; }
        public int Max { get; set; }
        public int Min { get; set; }
    }
}