﻿using CodeWars.Katas._6kyu;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodeWars.Tests.Katas._6kyu
{
    public class Source
    {
        public List<int> Expected { get; set; }
        public List<int> Numbers { get; set; }
    }

    [TestFixture]
    public class SumConsecutivesKataTests
    {
        private static readonly List<Source> SourceList = new List<Source>()
        {
            new Source()
            {
                Numbers = new List<int>() {1, 4, 4, 4, 0, 4, 3, 3, 1},
                Expected = new List<int>() {1, 12, 0, 4, 6, 1}
            },
            new Source() {Numbers = new List<int>() {-5, -5, 7, 7, 12, 0}, Expected = new List<int>() {-10, 14, 12, 0}},
            new Source() {Numbers = new List<int>() {1, 1, 7, 7, 3}, Expected = new List<int> {2, 14, 3}},
            new Source()
            {
                Numbers = new List<int>() {1, -1, -2, 2, 3, -3, 4, -4},
                Expected = new List<int>() {1, -1, -2, 2, 3, -3, 4, -4}
            },
            new Source() {Numbers = new List<int>() {1, 1, 1, 1, 1, 3}, Expected = new List<int>() {5, 3}}
        };

        [Test, TestCaseSource(nameof(SourceList))]
        public void Return_SumOfConsecutives(Source source)
        {
            Assert.AreEqual(source.Expected, SumConsecutivesKata.SumConsecutives(source.Numbers),
                "Returned wrong numbers. Case: " + string.Join(",", source.Numbers));
        }
    }
}