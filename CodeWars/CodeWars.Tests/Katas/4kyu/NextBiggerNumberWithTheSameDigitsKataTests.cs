﻿using CodeWars.Katas._4kyu;
using NUnit.Framework;

namespace CodeWars.Tests.Katas._4kyu
{
    [TestFixture]
    public class NextBiggerNumberWithTheSameDigitsKataTests
    {
        [TestCase(12, 21)]
        [TestCase(513, 531)]
        [TestCase(2017, 2071)]
        [TestCase(414, 441)]
        [TestCase(144, 414)]
        [TestCase(9, -1)]
        [TestCase(111, -1)]
        [TestCase(531, -1)]
        public void Return_NextBiggerNumberWithTheSameDigits(int number, int expected)
        {
            Assert.AreEqual(expected, NextBiggerNumberWithTheSameDigitsKata.NextBiggerNumber(number));
        }
    }
}