﻿using System.Collections.Generic;

namespace CodeWars.Katas._6kyu
{
    /*
    Backwards Read Primes are primes that when read backwards in base 10 (from right to left) are a different prime.
    (This rules out primes which are palindromes.)

    Examples:
    13 17 31 37 71 73 are Backwards Read Primes
    13 is such because it's prime and read from right to left writes 31 which is prime too. Same for the others.

    Find all Backwards Read Primes between two positive given numbers (both inclusive), the second one being greater than the first one.
    The resulting array or the resulting string will be ordered following the natural order of the prime numbers.

    backwardsPrime(2, 100) => "13 17 31 37 71 73 79 97"
    backwardsPrime(9900, 10000) => "9923 9931 9941 9967"
    */

    public class BackwardReadPrimesKata
    {
        public static string BackwardsPrime(long start, long end)
        {
            var numbers = new List<long>();
            if (start < 13) start = 13;
            for (var i = start; i <= end; i++)
            {
                if (!IsPrime(i)) continue;
                var reverseNumber = GetReverseNumber(i);
                if (IsPrime(reverseNumber) && i != reverseNumber)
                {
                    numbers.Add(i);
                }
            }
            return string.Join(" ", numbers);
        }

        private static long GetReverseNumber(long number)
        {
            long result = 0;
            while (number > 0)
            {
                result = result * 10 + number % 10;
                number /= 10;
            }
            return result;
        }

        private static bool IsPrime(long number)
        {
            if ((number & 1) == 0)
            {
                return number == 2;
            }
            for (var i = 3; (i * i) <= number; i += 2)
            {
                if ((number % i) == 0)
                {
                    return false;
                }
            }
            return number != 1;
        }
    }
}