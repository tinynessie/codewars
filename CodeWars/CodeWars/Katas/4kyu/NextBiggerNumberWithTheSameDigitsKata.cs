﻿using System.Linq;

namespace CodeWars.Katas._4kyu
{
    /*
        You have to create a function that takes a positive integer number and returns the next bigger number formed by the same digits:
        NextBiggerNumber(12)==21
        NextBiggerNumber(513)==531
        NextBiggerNumber(2017)==2071

        If no bigger number can be composed using those digits, return -1:

        NextBiggerNumber(9)==-1
        NextBiggerNumber(111)==-1
        NextBiggerNumber(531)==-1
    */

    public class NextBiggerNumberWithTheSameDigitsKata
    {
        public static long NextBiggerNumber(long n)
        {
            var sortedNumber = GetSortedNumber(n);
            for (var i = n + 1; i <= long.Parse(sortedNumber); i++)
            {
                if (sortedNumber == GetSortedNumber(i))
                {
                    return i;
                }
            }
            return -1;
        }

        public static string GetSortedNumber(long number)
        {
            return string.Join("", number.ToString().ToCharArray().OrderByDescending(x => x));
        }
    }
}