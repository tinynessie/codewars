﻿using System;
using System.Collections.Generic;
using CodeWars.Katas._4kyu;
using CodeWars.Katas._6kyu;

namespace CodeWars
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Console.WriteLine(string.Join(",", SumConsecutivesKata.SumConsecutives(new List<int>() {1, 1, 7, 7, 3})));
            //Console.WriteLine(BackwardReadPrimesKata.BackwardsPrime(1, 100));
            //Console.WriteLine(NextBiggerNumberWithTheSameDigitsKata.NextBiggerNumber(2017));
            Console.ReadKey();
        }
    }
}